package com.jordi.backend.apirest.auth;

public class JwtConfig {
	public static final String LLAVE_SECRETA ="alguna.clave.secreta.12345678";
	
	public static final String RSA_PUBLICA ="-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzodnbemCy8INg++i7JBZ\r\n" + 
			"kdxvAtn8BOn/WLjuiWFSOyW3yI3VW7NuTagY1TYMtu9b+nEtF5hqv8KYlmexh04h\r\n" + 
			"A85XhNpzPkNOXjRUUZftWsqmhfQRnmcs3NgpfZsC53NBWw3SMN6crMvgZm9ZtdAY\r\n" + 
			"3eBQGzj2oYi7fD2t6jCzUM9HxDpqtMwC4t6Lj+A4d9+L3SUWz6XpfCZFv4vCnbfx\r\n" + 
			"jRMd9RNTmddN3Hx6UYl/wTPByQ1s0yv09SJzevm0oLWv4+NrBZLuumjbNXUSTVKD\r\n" + 
			"x3UJvmQ6W7v151guBFta0wX/+4UwC8cR63DdTt3vWrrI29fjQ+y+62FxkRjydv9J\r\n" + 
			"ewIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";
	
	public static final String RSA_PRIVADA ="-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEpAIBAAKCAQEAzodnbemCy8INg++i7JBZkdxvAtn8BOn/WLjuiWFSOyW3yI3V\r\n" + 
			"W7NuTagY1TYMtu9b+nEtF5hqv8KYlmexh04hA85XhNpzPkNOXjRUUZftWsqmhfQR\r\n" + 
			"nmcs3NgpfZsC53NBWw3SMN6crMvgZm9ZtdAY3eBQGzj2oYi7fD2t6jCzUM9HxDpq\r\n" + 
			"tMwC4t6Lj+A4d9+L3SUWz6XpfCZFv4vCnbfxjRMd9RNTmddN3Hx6UYl/wTPByQ1s\r\n" + 
			"0yv09SJzevm0oLWv4+NrBZLuumjbNXUSTVKDx3UJvmQ6W7v151guBFta0wX/+4Uw\r\n" + 
			"C8cR63DdTt3vWrrI29fjQ+y+62FxkRjydv9JewIDAQABAoIBAAY4gd8Hp47nm2yb\r\n" + 
			"7TZe6rPABf/5/ONRAQX1HAo7SjTJ4A1EaGbIssvYAld5t0xTTVvvyMHrL1vzSpFN\r\n" + 
			"MkW3DruEnZI82jfH4zZnMRr8DoQrknR6c4Vn6iwEvssvvhimmhNaCTi7m9CAm5O6\r\n" + 
			"SuN1DyqvtPn870DSU4NcgD6LlPscFVEqSR8SJxxVf2v8e9LONW5ZNJ/RxHML0qzw\r\n" + 
			"XP1kuQzUW4KHKJzK0+JSMl4F6NiHl0TQibT+TnRU4dE9bihbT9zy7Tn8AsoIkbGG\r\n" + 
			"cb8iOmcKqoJBz9O7qaM4GJEbXcjiQ3+DzVwZtg6UrxTsdbxN6vd5mYx3wSeMCPxj\r\n" + 
			"Sf4MAvECgYEA+bSATUdggtTlgmglqs77ZwBDAHZydsGSEeRmOeHStQWPJfdXWgDl\r\n" + 
			"IpdoKTxxuY5TkzaXXkoWPlw/0foHuxOL+ORqmQ1UTaxpoKzHg6MnwF5quC+WNx7o\r\n" + 
			"Y14/ahC9iX1r1beVGZ1jzGQIKRGHkbwlXp8SIzCd5OX8GQVvTMGdZp0CgYEA07xC\r\n" + 
			"xEl2HGSGKmv0rWUw4ytj6Awd4qwEzlfFDpn0e1r6+fOINkv8MmIeV8wsaUliaagD\r\n" + 
			"my0BjcsHs6TWCnAunUlKXrDBxVxpFngPb1Xf3XEznfPrDSca+zUYRkJ5Y9jWjJiV\r\n" + 
			"dBO6Zm5pyQpM8Vy/hxs9coO01XFmUHM+/wS/6PcCgYADyYsQoP6DvXrlqXK4zn7d\r\n" + 
			"pxFdFm7SAkmW3+H1CsnJWRY8uy2l/dF73tQDnBkVeWUnHHLbSUBhHcESDDrFyZwb\r\n" + 
			"4EsFm0AmFXFcsIIuKvFVZ5kyxfX/TAa2Sx2nHC3jwfU6vpgSVWLIHNRK2ys1OKdQ\r\n" + 
			"chUyV3BtoldagcyYW29puQKBgQCeDLFqA8v+dasQj3Uc2aQcbo8s755/TRhpTmrx\r\n" + 
			"80njqo4ddgP/Q8kFcuhjcSzVisnDRH8snfPC/ALLvgdn00V4cvToqdh6kfh/pS8g\r\n" + 
			"VGG+0nlNNn4GW2JwkbmE+KnUNlAwstUcyH0jaNfeEapNoIX3PAYqLNYXXYaaFuy3\r\n" + 
			"P7xhEQKBgQDSz8ptz98JukuZnO/8Vjn8tQMy8YtRHSfKQbxreAHqS4N6cK7oh4pi\r\n" + 
			"/+jXeuANROLFydXCLXlzxx4tIleJKX8lQ0V7tQUYYqLqZ4luCpZYn78tbNa39HdN\r\n" + 
			"ypsBQp0Eyb/vz4rmN7/CJanzo5VX1NS99PZqauD1wWIL17XmAmI0iw==\r\n" + 
			"-----END RSA PRIVATE KEY-----";
}