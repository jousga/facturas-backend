package com.jordi.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jordi.backend.apirest.models.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
	//buscando por el nombre del método
	public Usuario findByUsername(String username);
/*	
	//buscando por más de un parámetro
	public Usuario findByUsernameAndEmail(String username, String email);

	//buscando usando una query a través de notación
	@Query("select u from Usuario u where u.username=?1")
	public Usuario findByUsername2(String username);
	
	//Buscando usando una notación query por más de un parámetro
	@Query("select u from Usuario u where u.username=?1 and u.otro=?2")
	public Usuario findByUsernameAndEmail2(String username, String email);
*/
}
